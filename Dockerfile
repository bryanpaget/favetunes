FROM python

RUN adduser favetunes

WORKDIR /home/favetunes

COPY ./requirements.txt ./

RUN pip install -r ./requirements.txt

COPY . .

USER favetunes

EXPOSE 8050

CMD [ "python", "./main.py" ]
