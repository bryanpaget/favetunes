#!/usr/bin/env python

import functools
import dash_table
from datetime import datetime as dt

from favetunes.favetunes import FaveTunesApplication
from favetunes.widgets import Content

from favetunes.widgets import TabbedContainer
from favetunes.widgets import NowPlaying
from favetunes.widgets import left_tab_style
from favetunes.widgets import right_tab_style

import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from favetunes.index import index
from favetunes.work import work

import plotly.express as px




# TODO: Add groupby functionality that when selected, shows the number of plays per day / week / month
# as a barplot. i.e. replace the treemap with the barplot when groupby active.




favetunes = FaveTunesApplication("./secrets/secrets.yaml")

favetunes.fetch_scrobbles(limit=999)  # TODO: only fetch upon first visit to music page.




date_picker_range = dcc.DatePickerRange(
        id='date-picker-range',
        min_date_allowed=favetunes.first_scrobble_date,
        max_date_allowed=dt.today(),
        initial_visible_month=dt.today(),
        end_date=dt.today(),
        className="dcc_control",
        with_portal=True,
        start_date_placeholder_text="Start Date",
        end_date_placeholder_text="End Date"
)



history = html.Div(children=[
    
    html.H4("Filter by Artist:", className="control_label"),
    
    html.Div([
        
        dcc.Dropdown(
        id="artist-selection",
        options=favetunes.artist_options,
        multi=False,
        clearable=False,
        className="dcc_control",
        placeholder="All Artists"
        )
        
    ]),

    html.H4("Filter by Date Range:", className="control_label"),
    
    html.Div(children=[date_picker_range], className="content"),

    html.Div([
        dcc.Graph(id='treemap-figure'),
        html.Div([
            dash_table.DataTable(
                id="music-table",
                columns=[{"name": i, "id": i} for i in favetunes.scrobbles.columns],
                page_current=0,
                page_size=10
            )
        ])
    ])

], className="history")































sidebar = html.Div(
    [
        html.Div([html.Img(src="/assets/img/me.jpg")], className="portrait"),
        html.H2("Bryan Paget", className="signature"),
                dbc.NavLink("Life", href="/page-1", id="page-1-link"),
                html.Br(),
                dbc.NavLink("Experience", href="/page-2", id="page-2-link"),
                html.Br(),
                dbc.NavLink("Images", href="/page-3", id="page-3-link"),
                html.Br(),
                dbc.NavLink("Music", href="/page-4", id="page-4-link"),
                html.Br(),
                dbc.NavLink("Writing", href="/page-5", id="page-5-link"),
    ], className="navigation_container"
)

content = html.Div(id="page-content", className="main-content")

favetunes.layout = html.Div([dcc.Location(id="url"), sidebar, content], className="content-foundation")


# this callback uses the current pathname to set the active state of the
# corresponding nav link to true, allowing users to tell see page they are on
@favetunes.callback(
    [Output(f"page-{i}-link", "active") for i in range(1, 6)],
    [Input("url", "pathname")],
)
def toggle_active_links(pathname):
    if pathname == "/":
        # Treat page 1 as the homepage / index
        return True, False, False
    return [pathname == f"/page-{i}" for i in range(1, 6)]


@favetunes.callback(
    Output("page-content", "children"),
    [Input("url", "pathname")]
)
def render_page_content(pathname):
    if pathname in ["/", "/page-1"]:
        return index
    elif pathname == "/page-2":
        return work
    elif pathname == "/page-3":
        return index
    elif pathname == "/page-4":
        return music
    elif pathname == "/page-5":
        return html.P("Oh cool, this is page 5!")
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.Div([html.Img(src="/assets/img/operator.png")], className="centered-image"),
            html.H1("404: Not found", className="text-danger"),
            html.P(f"The pathname {pathname} was not recognised..."),
        ], className="four-oh-four"
    )


music = TabbedContainer(children=[
    dcc.Store(id="aggregate_data"),
    html.Div(id="output-clientside"),

    dcc.Tabs(
        id="main-tabs",
        value="now-playing",
        children=[

            dcc.Tab(
                label="Now Playing",
                value="now-playing",
                style=left_tab_style,
                selected_style=left_tab_style,
                disabled_style=left_tab_style,
                children=[NowPlaying(favetunes.get_now_playing()).render()]
            ),

            dcc.Tab(
                
                label="Playback History",
                
                value="playback-history",
                
                style=right_tab_style,
                selected_style=right_tab_style,
                disabled_style=right_tab_style,
                children=[history])        
            ]),
    ]
)







@functools.lru_cache(maxsize=32)
@favetunes.callback(
        Output(component_id="music-table", component_property="data"),
        [
            Input(component_id="date-slider", component_property="value"),
            Input(component_id="artist-selection", component_property="value"),
        ]
    )
def update_data(date_range_slider, artist):
    return favetunes.date_filter(date_range_slider, artist).to_dict("records")




@favetunes.callback(
    Output("treemap-figure", "figure"),
    [
        Input('date-picker-range', 'start_date'),
        Input('date-picker-range', 'end_date'),
        Input("artist-selection", "value")
    ]
)
def render_treemap(start_date, end_date, artist):

    dff = favetunes.date_filter((start_date, end_date), artist)
    dff["Music"] = "Music"  # Music is the root node
    
    print(artist)
    print(start_date)
    print(end_date)
    print(dff)

    return px.treemap(
        dff,
        path=["Music", "Artist", "Album", "Title"],  # TODO: genre!
        values="Count",
        hover_data=["Play Date"],
        height=600,
        title="Artists, Albums and Songs",
    )




if __name__ == "__main__":

    favetunes.run_server(
        host="0.0.0.0",
        port="8050"
    )