#!/usr/bin/env python

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from favetunes.widgets import WrittenWord
from favetunes.widgets import Content

from favetunes.lorem import lorem

index = Content(children=[

    WrittenWord(children=[
        html.H1("The Cities I Have Called Home"),
        html.H2("Toronto"),

        html.Div([
            html.Img(src="/assets/img/toronto.jpg")],
            className="border centered image rounded-corners"),

        html.P(lorem[0]),
        html.P(lorem[1]),
        html.P(lorem[8])
    ]),

    WrittenWord(children=[
        html.H2("Vancouver"),
        html.Div([html.Img(src="/assets/img/vancouver.jpg")], className="border centered image rounded-corners"),
        html.P(lorem[2]),
        html.P(lorem[3]),
        html.P(lorem[9])
    ]),
    WrittenWord(children=[
        html.H2("London"),
        html.Div([html.Img(src="/assets/img/london.jpg")], className="border centered image rounded-corners"),
        html.P(lorem[4]),
        html.P(lorem[5]),
        html.P(lorem[10])
    ]),
    WrittenWord(children=[

        html.H2("Ottawa"),
        html.Div([html.Img(src="/assets/img/ottawa.jpg")], className="border centered image rounded-corners"),
        html.P(lorem[6]),
        html.P(lorem[7])
    ])

])
