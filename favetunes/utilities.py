#!/usr/bin/env python

import datetime as dt

class Date:
    """Formats date correctly.

    Turn this: 28 Feb 2020 00:09 GMT into something more useful.

    """

    MONTHS = {
        "Jan": 1,
        "Feb": 2,
        "Mar": 3,
        "Apr": 4,
        "May": 5,
        "Jun": 6,
        "Jul": 7,
        "Aug": 8,
        "Sep": 9,
        "Oct": 10,
        "Nov": 11,
        "Dec": 12,
    }

    def __init__(self, date_as_string):
        date_split = date_as_string.split(" ")
        time_split = date_split[3].split(":")
        self.year = int(date_split[2])
        self.month = self.month_to_int(date_split[1])
        self.day = int(date_split[0])
        self.hour = int(time_split[0])
        self.minute = int(time_split[1])

    def __str__(self):
        return f"{self.day} {self.month} {self.hour}:{self.minute}"

    def month_to_int(self, month):
        """Turn a month into an Integer."""
        month = month[:3]
        n = self.MONTHS.get(month)
        return n
    
    def get_date(self, time_of_day=False):
        """Return datetime object.

        """
        if time_of_day:
            ret = dt.datetime(
                year=self.year,
                month=self.month,
                day=self.day,
                hour=self.hour,
                minute=self.minute
            )
        else:
            ret = dt.datetime(
                year=self.year,
                month=self.month,
                day=self.day
            )
        return ret