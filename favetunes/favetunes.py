#!/usr/bin/env python

"""Main library of FaveTunes classes and functions."""

import functools
import pandas as pd
import pylast
import yaml
import dash

import numpy as np

from favetunes.utilities import Date


class FaveTunesApplication(dash.Dash):

    _scrobbles = None

    def __init__(self, secrets):
        super().__init__(__name__, suppress_callback_exceptions=True)

        with open(secrets, "r") as f:
            secrets = yaml.load(f, Loader=yaml.SafeLoader)

        network = pylast.LibreFMNetwork(
            password_hash=secrets["password_hash"],
            username=secrets["username"]
        )

        self._user = pylast.User(secrets["username"], network)

    def fetch_scrobbles(self, **kwargs):

        scrobbles = self._user.get_recent_tracks(**kwargs)

        scrobbles = pd.DataFrame([
            {
                "Artist": elem.track.artist.name,
                "Title": elem.track.title,
                "Album": elem.album,
                "Play Date": elem.playback_date
            } for elem in scrobbles
        ])

        scrobbles["Play Date"] = (scrobbles["Play Date"]
                                  .apply(lambda x: Date(x).get_date(time_of_day=False)))

        scrobble_counts = scrobbles.groupby("Title").size().reset_index()
        scrobble_counts.columns = ["Title", "Count"]

        self._scrobbles = pd.merge(
            scrobbles.drop_duplicates(),
            scrobble_counts,
            how="inner",
            on="Title"
        )

    @property
    def scrobbles(self):
        return self._scrobbles

    @functools.cached_property
    def scrobble_dates(self):
        return pd.Series(reversed(self.scrobbles["Play Date"]))

    @functools.cached_property
    def artist_options(self):
        artists = self.scrobbles["Artist"].unique()
        artists = np.append(artists, "All Artists")
        artists = sorted(artists)
        return [{"label": artist, "value": str(i)} for (i, artist) in enumerate(artists)]

    @property
    def layout(self):
        return self._layout

    @layout.setter
    def layout(self, layout):
        self._layout = layout

    def get_now_playing(self):
        return self._user.get_now_playing()

    @functools.cached_property
    def first_scrobble_date(self):
        return self.scrobbles["Play Date"].min()

    @functools.cached_property
    def last_scrobble_date(self):
        return self.scrobbles["Play Date"].max()

    def scrobbles_per_day(self):
        raise NotImplementedError("Not implemented yet!")

    @functools.cached_property
    def marks_for_slider(self):

        _length = len(self.scrobble_dates)

        mod = int(_length/10)

        marks = {
            x: f"{y.day}/{y.month}/{y.year}"
            for (x, y)
            in zip(range(_length), self.scrobble_dates)
            if x % mod == 0
        }

        end_date = self.scrobble_dates[len(self.scrobbles) - 1]
        marks[str(len(self.scrobbles) - 1)] = f"{end_date.day}/{end_date.month}/{end_date.year}"
        
        return marks

    def get_tags(self):
        raise NotImplementedError("Not implemented yet.")

    def date_filter(self, date_slider, artist):
                
        if artist not in [0, None]:
            artist = [self.artist_options[int(x)] for x in artist]
        else:
            artist = "All Artists"

        if date_slider[0] is not None:
            small_date = date_slider[0]
        else:
            small_date = self.first_scrobble_date
            
        if date_slider[1] is not None:
            large_date = date_slider[1]
        else:
            large_date = self.last_scrobble_date

        dff = self.scrobbles[
            (self.scrobbles["Play Date"] > small_date) &
            (self.scrobbles["Play Date"] < large_date)
        ]
        
        if artist != "All Artists":

            artist = [x["label"] for x in artist]
            dff = dff[dff["Artist"].isin(artist)]

        return dff