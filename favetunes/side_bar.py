import os

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from index import index
from work import work

app = dash.Dash(__name__)

sidebar = html.Div(
    [
        html.Div([html.Img(src="/assets/img/me.jpg")], className="portrait"),
        html.H2("Bryan Paget", className="signature"),
        html.Div([
            html.P("A fun guy.", className="lead"),
            dbc.Nav(
                [
                    dbc.NavLink("Life", href="/page-1", id="page-1-link"),
                    html.Br(),
                    dbc.NavLink("Work", href="/page-2", id="page-2-link"),
                    html.Br(),
                    dbc.NavLink("School", href="/page-3", id="page-3-link"),
                    html.Br(),
                    dbc.NavLink("Music", href="/page-4", id="page-4-link"),
                    html.Br(),
                    dbc.NavLink("Writing", href="/page-5", id="page-5-link"),
                ],
                vertical=True,
            ),
        ], className="navigation")
    ],
    className="side_bar"
)

content = html.Div(id="page-content", className="content")

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])


# this callback uses the current pathname to set the active state of the
# corresponding nav link to true, allowing users to tell see page they are on
@app.callback(
    [Output(f"page-{i}-link", "active") for i in range(1, 6)],
    [Input("url", "pathname")],
)
def toggle_active_links(pathname):
    if pathname == "/":
        # Treat page 1 as the homepage / index
        return True, False, False
    return [pathname == f"/page-{i}" for i in range(1, 6)]


@app.callback(
    Output("page-content", "children"),
    [Input("url", "pathname")]
)
def render_page_content(pathname):
    if pathname in ["/", "/page-1"]:
        return index
    elif pathname == "/page-2":
        return work
    elif pathname == "/page-3":
        return work
    elif pathname == "/page-4":
        return music
    elif pathname == "/page-5":
        return html.P("Oh cool, this is page 5!")
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.Div([html.Img(src="/assets/img/operator.png")], className="centered-image"),
            html.H1("404: Not found", className="text-danger"),
            html.P(f"The pathname {pathname} was not recognised..."),
        ], className="four-oh-four"
    )


if __name__ == "__main__":
    app.run_server(port=8888)
