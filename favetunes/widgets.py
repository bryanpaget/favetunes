#!/usr/bin/env python

import dash_html_components as html
import wikipediaapi
import coverpy
import dash_core_components as dcc




left_tab_style = {
    "border-radius": "8px 0px 0px 0px",
    "border-width": "0 1px 1px 0"
}

right_tab_style = {
    "border-radius": "0px 8px 0px 0px",
    "border-width": "0 0 1px 1px"
}


class Content(html.Div):

    def __init__(self, **kwargs):
        super().__init__(className="content", **kwargs)


class Article(html.Div):

    def __init__(self, **kwargs):
        super().__init__(className="container", **kwargs)


class WrittenWord(html.Div):

    def __init__(self, **kwargs):
        super().__init__(className="written-word", **kwargs)


class Container(html.Div):
    
    def __init__(self, **kwargs):
        super().__init__(className="container border", **kwargs)


class TabbedContainer(html.Div):
    
    def __init__(self, **kwargs):
        super().__init__(className="tabbed container border", **kwargs)


class NowPlaying:

    def __init__(self, now_playing):

        self._wiki = wikipediaapi.Wikipedia('en')
        self._coverpy = coverpy.CoverPy()

        try:
            self.listener_count = now_playing.get_listener_count()
            self._name = "by " + now_playing.artist.name
            self._title = now_playing.title
            self._album_art = self._coverpy.get_cover(" ".join([self._title, self._name])).artwork(256)
        except:
            self.listener_count = ""
            self._name = "I'm not listening to anything right now."
            self._title = ""
            self._album_art = "/assets/img/chicken.jpg"

    @property
    def wikipedia_summary(self):

        key_terms = ["", "band", "musician"]

        for term in key_terms:
            page = self._wiki.page(f"{self._name} {term}")
            if (len(page.summary[0:20])) > 0:
                break

        return page.summary[0:1800]

    @property
    def cover_art(self, limit=1):
        return html.Img(src=self._album_art)

    def render(self):
        return html.Div([
            html.Div([
                html.Div([
                    html.Div([self.cover_art], className="centered-image rounded-corners"),
                    html.H3(self._title),
                    html.H5(self._name)
                ]),
                html.P(self.wikipedia_summary)
            ])
        ], className="now_playing_container")




class CuteBox(html.Div):
    
    def __init__(self):
        pass