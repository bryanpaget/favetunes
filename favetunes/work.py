#!/usr/bin/env python

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from favetunes.widgets import TabbedContainer
from dash.dependencies import Input, Output
from favetunes.widgets import left_tab_style
from favetunes.widgets import right_tab_style


# TODO: Add buttons. Make the resume section interactive.
# There will be a button which 


class Work:
    def __init__(self, place, role, when):
        self.place = place
        self.role = role
        self.when = when
        
        
class Education:
    def __init__(self, place, degree, when):
        self.place = place
        self.degree = degree
        self.when = when


WORK_DATA = [
    Work("Statistics Canada", "Data Scientist", "2019"),
    Work("Cheetah Networks", "Data Scientist", "2019"),
    Work("Friends of Canadian Broadcasting", "Data Scientist", "2018-2019"),
    Work("uOttawa (Dr. Kelly Burkett)", "C++ Programmer", "2018"),
    Work("Advanced Symbolics", "AI Intern", "2018"),
    Work("uOttawa", "Teaching Assistant", "2017"),
    Work("uOttawa (Dr. Diana Inkpen)", "NLP Research Assistant", "2015-2017")
]

EDUCATION_DATA = [
    Education("University of Ottawa", "MSc. Statistics", "2019"),
    Education("University of Ottawa", "BSc. Statistics", "2019"),
]

def education_table(data):
    table = [html.Tr([
        html.Th("Place"),
        html.Th("Degree"),
        html.Th("When")
        ])]
    table.extend([
        html.Tr([html.Td(experience.place),
                 html.Td(experience.degree),
                 html.Td(experience.when)])
        for experience in data
    ])
    return html.Table(table)


def work_experience_table(data):
    table = [html.Tr([
        html.Th("Place"),
        html.Th("Role"),
        html.Th("When")
        ])]
    table.extend([
        html.Tr([html.Td(job.place),
                 html.Td(job.role),
                 html.Td(job.when)])
        for job in data
    ])
    return html.Table(table)


work = TabbedContainer(children=[

    dcc.Tabs(
        id="main-tabs",
        value="work",
        children=[
            dcc.Tab(
                label="Work Experience",
                value="work",
                style=left_tab_style,
                selected_style=left_tab_style,
                disabled_style=left_tab_style,
                children=[
                    work_experience_table(WORK_DATA)]
            ),
    dcc.Tab(
        label="Education",
        value="education",
        style=right_tab_style,
        selected_style=right_tab_style,
        disabled_style=right_tab_style,
        children=[
            education_table(EDUCATION_DATA)
])
    ])
    ])