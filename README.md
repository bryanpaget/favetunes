# GNOME Scrobbler

Analyze your music listening habits.

## Types of visualizations:

### Network Graphs

Graph connections -- artist are connected if you listened to one and then the other.

Graph connections can be bidirectional if you listen to artist A and then artist B
and you listen to artist B before artist A


